
--
-- Dump of data for the table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `user_pasword`) VALUES
('1040567322',  'Bill Gates', '12340'),
('1041992002', 'Steve Jobs', '12345'),
('0287323781',  'Brad Pitt', '54321'),
('837432423',  'Angelina Jolie', '13256'),
('32964907',  'Alberto Vaho', '41235' );
-- -----------------------------------------------------

--
-- Dump of data for the table `buy`
--

INSERT INTO `buy` (`id_product`, `name_product`, `date_buy`, `id_user`, `name_provider`, `state_buy`) VALUES
('001', 'Queso', '2020-04-01', '1040567322', 'Colanta', 'Pendiente'),
('002', 'Leche', '2020-04-02', '1041992002', 'Colanta', 'Pendiente'),
('003', 'Yogurt', '2020-04-03', '0287323781', 'Alqueria', 'Cancelado'),
('004', 'Kumis', '2020-04-02', '837432423', 'Alqueria', 'Entregado' );


-- -----------------------------------------------------

--
-- Dump of data for the table `sale`
--

INSERT INTO `sale` (`id_product`, `name_product`, `date_sale`, `id_user`, `state_sale`, `name_customer`) VALUES
('001', 'Queso', '2020-04-01', '837432423', 'Exito', 'Pendiente'),
('002', 'Leche', '2020-04-02', '837432423', 'Carulla', 'Pendiente'),
('003', 'Yogurt', '2020-04-03', '32964907', 'Carulla', 'Cancelado'),
('004', 'Kumis', '2020-04-02', '32964907', 'Euro', 'Entregado' );



-- -----------------------------------------------------