-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dcerp
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dcerp` ;

-- -----------------------------------------------------
-- Schema dcerp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dcerp` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `dcerp` ;

-- -----------------------------------------------------
-- Table `dcerp`.`buy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dcerp`.`buy` (
  `id` INT NOT NULL,
  `provider_name` VARCHAR(45) NOT NULL,
  `provider_address` VARCHAR(45) NOT NULL,
  `provider_number` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dcerp`.`buy_line`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dcerp`.`buy_line` (
  `id` INT NOT NULL,
  `id_product` INT NOT NULL,
  `product_name` VARCHAR(45) NOT NULL,
  `product_cost` INT NOT NULL,
  `product_quantity` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_buy_line_buy1_idx` (`id` ASC),
  CONSTRAINT `fk_buy_line_buy1`
    FOREIGN KEY (`id`)
    REFERENCES `dcerp`.`buy` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dcerp`.`sale`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dcerp`.`sale` (
  `id` INT NOT NULL,
  `customer_name` VARCHAR(45) NOT NULL,
  `customer_address` VARCHAR(45) NOT NULL,
  `customer_number` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dcerp`.`sale_line`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dcerp`.`sale_line` (
  `id` INT NOT NULL,
  `id_product` INT NOT NULL,
  `product_name` VARCHAR(45) NOT NULL,
  `product_cost` INT NOT NULL,
  `product_quantity` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sale_line_sale_idx` (`id` ASC),
  CONSTRAINT `fk_sale_line_sale`
    FOREIGN KEY (`id`)
    REFERENCES `dcerp`.`sale` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dcerp`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dcerp`.`user` (
  `id_user` INT NOT NULL,
  `user_name` VARCHAR(45) NOT NULL,
  `user_password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
