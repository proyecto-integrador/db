FROM mysql

MAINTAINER dcaballero "d@dcaballero.net"
MAINTAINER smunoz "smunoz@dcaballero.net"

VOLUME ["/var/lib/mysql"]

ADD ./conf/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf

COPY ./dcerp.sql /docker-entrypoint-initdb.d/

EXPOSE 3306
